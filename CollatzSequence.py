# https://automatetheboringstuff.com/2e/chapter3/
# The Collatz Sequence

def collatz(number):
    while number != 1:
        if number%2 == 0:
            number = number//2
            print(number)
            continue
        if number%2 == 1:
            number = 3*number+1
            print(number)
            continue

input = int(input('Enter a positive integer: '))

collatz(input) 
