#!/bin/env python3

"""
Program takes input from user to guess a random integer.  Then proceeds to tell them if their int is too low or to high.
"""

import random

guess_count = 0
number_to_guess = random.randint(0,20)

"""accept guesses until they figure out the answer"""
while True:
    guess = input("Guess a # between 0 and 100(or q to quit): ")
    try:
        guess = int(guess)
    except ValueError:
        if guess in ('q','Q'):
            print("sorry to see you go, the correct answer was: %s" % number_to_guess)
            break
        print("Sorry you must only use integers between 0 and 100")
        continue
    guess_count = guess_count + 1
    if guess == number_to_guess:
        print("SUCCESS! You won in {0} guesses".format(guess_count))
        break
    elif guess > number_to_guess:
        print("sorry, your guess is larger than the correct #")
    else:
        print("sorry, your guess is smaller than the correct #")
