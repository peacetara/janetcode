# Write your code here :-)
 
import random, itertools
 
print("I am thinking of a number between 1 and 20.")
print("Take a guess.")
 
blackbox = random.randint(1, 21)
guess = float(input())
tracker = 1
 
 
while guess != blackbox:
    if guess < blackbox:
        print("Your guess is too low.")
        print("Take a guess.")
        guess = float(input())
        tracker = tracker + 1
        continue
 
    if guess > blackbox:
        print("Your guess is too high.")
        print("Take a guess.")
        guess = float(input())
        tracker = tracker + 1
        continue
 
if guess == blackbox:
    print(
        "Good job! You guessed my number! It only took you " + str(tracker) + " tries.")
 
