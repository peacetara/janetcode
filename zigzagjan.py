# This is my attempt of the textbook's version of the code before discovering it. 
# Find the textbook's version at zigzag.py

import sys

try:
    while True:
        print("    ********")
        print("   ******** ")
        print("  ********  ")
        print(" ********   ")
        print("********    ")
        print(" ********   ")
        print("  ********  ")
        print("   ******** ")
except KeyboardInterrupt:
    sys.exit()
